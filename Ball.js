function Ball() {
    this.gravity = 0.6
    this.horVelocity = 0
    this.vertVelocity = 0
    this.bounce = -0.2
    this.radius = 15
    this.collided = false

    this.spawn = function() {
        //spawna random mellan 0,4<x<0,6, 0<y<0,2
        this.y = random(height/8)
        this.x = random(width/5, width * 4 / 5)
    }

    this.show = function () {
        fill(255)
        ellipse(this.x, this.y, 2*this.radius, 2*this.radius)
    }

    this.update = function(leftFlipper, rightFlipper) {
        this.vertVelocity += this.gravity
        this.y += this.vertVelocity
        this.x += this.horVelocity
        this.checkCollision(leftFlipper, rightFlipper)
    }

    this.checkCollision = function(leftFlipper, rightFlipper) {

        if(collideLineCircle(leftFlipper.startX, leftFlipper.startY,
            leftFlipper.tipX, leftFlipper.tipY, this.x, this.y,
            2.5*this.radius)) {
            if(!this.collided) {
                let alpha = Math.atan((leftFlipper.tipX - leftFlipper.startX) / (leftFlipper.tipY - leftFlipper.startY))
                let vy =  Math.cos(alpha) * this.horVelocity - Math.sin(alpha) * this.vertVelocity
                let vx = Math.cos(alpha) * this.horVelocity + Math.sin(alpha) * this.vertVelocity
                this.horVelocity = vx * Math.sin(alpha) + vy * Math.cos(alpha)
                this.vertVelocity = - vx * Math.cos(alpha) - vy  * Math.sin(alpha)
                this.collided = true
            }
        } else if(collideLineCircle(rightFlipper.startX, rightFlipper.startY,
            rightFlipper.tipX, rightFlipper.tipY, this.x, this.y,
            2.5*this.radius)) {
            if(!this.collided) {
                let alpha = Math.atan((rightFlipper.tipX - rightFlipper.startX) / (rightFlipper.tipY - rightFlipper.startY))
                let vy =  Math.cos(alpha)* this.horVelocity - Math.sin(alpha) * this.vertVelocity
                let vx = Math.cos(alpha) * this.horVelocity + Math.sin(alpha) * this.vertVelocity
                this.horVelocity =  - vx * Math.sin(alpha) + vy * Math.cos(alpha)
                this.vertVelocity = - vx * Math.cos(alpha) + vy  * Math.sin(alpha)
                this.collided = true
                console.log(Math.cos(alpha), Math.sin(alpha))
            }
        } else {
            this.collided = false
        }

        if (this.y > height - this.radius) {
            this.y = height - this.radius
            this.vertVelocity *= this.bounce
        }

        if (this.y < this.radius) {
            this.y = this.radius
            this.vertVelocity *= this.bounce
        }

        if (this.x > width - this.radius) {
            this.x = width - this.radius
            this.horVelocity *= this.bounce
        }

        if (this.x < this.radius) {
            this.x = this.radius
            this.horVelocity *= this.bounce
        }
    }

    this.right = function() {
        this.horVelocity += 1
    }

    this.left = function() {
        this.horVelocity -= 1
    }

    this.up = function() {
        this.vertVelocity -= 10
    }

    this.down = function() {
        this.vertVelocity += 10
    }

}
