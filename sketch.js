
var ball
var leftFlipper
var rightFlipper


function setup() {
    createCanvas(400,600)
    ball = new Ball()
    ball.spawn()
    rightFlipper = new Flipper('right')
    leftFlipper = new Flipper('left')
}

function draw() {
    background(0)
    ball.update(leftFlipper, rightFlipper)
    ball.show()
    drawSides()
    rightFlipper.show()
    leftFlipper.show()
}

function drawSides() {
    strokeWeight(5)
    stroke(color('white'), 1)
    line(0, height * 4 / 5, width/5, height)
    line(width, height * 4 / 5, width * 4 / 5, height)
    stroke(color('white'), 1)
}

function keyPressed() {
    if (keyCode == UP_ARROW) {
        ball.up()
    } else if(keyCode == RIGHT_ARROW) {
        ball.right()
    } else if(keyCode == LEFT_ARROW) {
        ball.left()
    } else if(keyCode == DOWN_ARROW) {
        ball.down()
    } else if(keyCode == '32') {
        leftFlipper.flip()
    } else if(keyCode == ENTER) {
        rightFlipper.flip()
    }
}
