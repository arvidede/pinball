function Flipper(direction) {

    if(direction == 'left') {
        this.startX = width/10
        this.tipX = width * 2 / 5
        this.flipX = width/3
    } else {
        this.startX = width * 9 / 10
        this.tipX = width * 3 / 5 // glöm inte glugg i mitten
        this.flipX = width * 2 / 3
    }

    this.startY = height * 0.9
    this.tipY = height
    this.flipY = 0.85 * height
    this.downMotion = false
    this.upMotion = false

    this.show = function() {
        strokeWeight(5)
        line(this.startX, this.startY, this.tipX, this.tipY)
        stroke(color('white'), 1)

        if(this.upMotion) {
            if(this.tipY < this.flipY) {
                this.tipY -= 15
            } else {
                this.tipY = this.flipY
                this.upMotion = false
                this.downMotion = true
            }
        } else if(this.downMotion){
            if(this.tipY < height) {
                this.tipY += 15
            } else {
                this.tipY = height
                this.downMotion = false
            }
        }
    }

    this.flip = function() {
        this.upMotion = true
    }

    this.kVal = function() {
        return (this.startY - this.tipY) / (this.startX - this.tipY)
    }
}
